
public class Data {
    private int dia;
    private int mes;
    private int ano;
    
    public Data(){}

    /*métodos de acesso de dia*/
    public int getDia() {
        return dia;
    }
    public void setDia(int dia) {
        this.dia = dia;
    }

    /*métodos de acesso de mes*/
    public int getMes() {
        return mes;
    }
    public void setMes(int mes) {
        this.mes = mes;
    }

    /*métodos de acesso de ano*/
    public int getAno() {
        return ano;
    }
    public void setAno(int ano) {
        this.ano = ano;
    }
}
